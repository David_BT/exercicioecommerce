package br.com.siteVendas.validadores;

import br.com.siteVendas.exceptions.RGInconsistenteException;

public class ValidadorRG extends ValidadorDocumento {

	@Override
	public void validadorDocumento(String representacao) throws RGInconsistenteException {
		
		if (representacao.equals("")) {
			throw new RGInconsistenteException("RG informado não é valido");
		}
	
	}

}
