package br.com.siteVendas.validadores;

import br.com.siteVendas.exceptions.CpfInconsistenteException;

public class ValidadorCPF extends ValidadorDocumento {
	
	@Override
	public void validadorDocumento(String representacao) throws CpfInconsistenteException{
		
		if (representacao.equals("")) {
			throw new CpfInconsistenteException("CPF informado não é valido");
		}
	}

}
