package br.com.siteVendas.validadores;

import br.com.siteVendas.enums.TipoDocumentoEnum;
import br.com.siteVendas.exceptions.CpfInconsistenteException;
import br.com.siteVendas.exceptions.RGInconsistenteException;

public abstract class ValidadorDocumento {

	public abstract void validadorDocumento (String representacao) throws RGInconsistenteException, CpfInconsistenteException ;
	
	public static ValidadorDocumento getInstace(TipoDocumentoEnum tipoDocumento) {
		ValidadorDocumento res = null;
		
		switch (tipoDocumento) {
		case RG:
			res = new ValidadorRG();
			break;

		case CPF:
			res = new ValidadorCPF();
			break;
			
		default:
			break;
		}
		
		return res;
	}
	
}
