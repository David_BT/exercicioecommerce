package br.com.siteVendas.validadores;

import br.com.siteVendas.exceptions.CepInconsistenteException;

public interface ValidadorCEP {

	public static void validarCEP (String CEP) throws CepInconsistenteException{
		
		if (CEP.equals("")) {
			throw new CepInconsistenteException("CEP não encontrado");
		}
	}

}
