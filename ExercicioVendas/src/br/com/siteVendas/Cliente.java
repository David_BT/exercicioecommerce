package br.com.siteVendas;

import java.util.ArrayList;
import java.util.List;

public class Cliente {
	private String rg;
	private String cpf;
	
	private List<Endereco> listaEnderecos;
	
	public Cliente() {
		super();
		
		listaEnderecos = new ArrayList<Endereco>();
	}

	public void addAddress(Endereco endereco) {
		
		listaEnderecos.add(endereco);

	}

	public String getRg() {
		return rg;
	}
	
	public void setRg(String rg) {
		this.rg = rg;
	}
	
	public String getCpf() {
		return cpf;
	}
	
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
}
