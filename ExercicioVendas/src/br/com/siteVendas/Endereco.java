package br.com.siteVendas;

import br.com.siteVendas.exceptions.CepInconsistenteException;
import br.com.siteVendas.validadores.ValidadorCEP;

public class Endereco {

	private String rua;
	private Integer numero;
	private String cep;
	private String bairro;
	private String cidade;
	private String estado;
	
	public String getRua() {
		return rua;
	}
	
	public void setRua(String rua) {
		this.rua = rua;
	}
	
	public Integer getNumero() {
		return numero;
	}
	
	public String getCep() {
		return cep;
	}

	public void setCep(String cep) throws CepInconsistenteException {
		ValidadorCEP.validarCEP(cep);
		
		this.cep = cep;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}
		
	public String getBairro() {
		return bairro;
	}
	
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	
	public String getCidade() {
		return cidade;
	}
	
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	
	public String getEstado() {
		return estado;
	}
	
	public void setEstado(String estado) {
		this.estado = estado;
	}
}
