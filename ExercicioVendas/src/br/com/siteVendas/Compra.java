package br.com.siteVendas;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import br.com.siteVendas.exceptions.ClienteNaoEncontradoException;

public class Compra {
	
	private Cliente cliente;
	private List<ItemCompra> listaItens;
	private BigDecimal totalValue;
	
	private BancoClientes clientBank;
	
	public Compra() {
		super();
		
		listaItens = new ArrayList<ItemCompra>();
	}
	
	public void logginCliente(Cliente cliente) throws ClienteNaoEncontradoException{
		
		this.cliente = clientBank.findClient(cliente);
	}
	
	
	public void payment() {
		
		if (cliente == null) {
			System.out.println("Cadastrar cliente");
			// lançar erro informando que cliente precisa estar cadastrado
		}
		
		
	}
	
	
	// Getter & Setters
		
	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public List<ItemCompra> getListaItens() {
		return listaItens;
	}

	public void setListaItens(List<ItemCompra> listaItens) {
		this.listaItens = listaItens;
	}
}
