package br.com.siteVendas.exceptions;

public class CpfInconsistenteException extends eCommerceException {

	public CpfInconsistenteException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CpfInconsistenteException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public CpfInconsistenteException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public CpfInconsistenteException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public CpfInconsistenteException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
