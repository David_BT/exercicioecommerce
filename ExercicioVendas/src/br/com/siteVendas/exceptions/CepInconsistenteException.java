package br.com.siteVendas.exceptions;

public class CepInconsistenteException extends eCommerceException {

	public CepInconsistenteException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CepInconsistenteException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public CepInconsistenteException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public CepInconsistenteException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public CepInconsistenteException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
