package br.com.siteVendas.exceptions;

public class eCommerceException extends Exception {

	public eCommerceException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public eCommerceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public eCommerceException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public eCommerceException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public eCommerceException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
