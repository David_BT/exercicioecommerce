package br.com.siteVendas.exceptions;

public class RGInconsistenteException extends eCommerceException {

	public RGInconsistenteException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RGInconsistenteException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public RGInconsistenteException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public RGInconsistenteException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public RGInconsistenteException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
