package br.com.siteVendas.exceptions;

public class ClienteNaoEncontradoException extends eCommerceException {

	public ClienteNaoEncontradoException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ClienteNaoEncontradoException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public ClienteNaoEncontradoException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ClienteNaoEncontradoException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ClienteNaoEncontradoException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
