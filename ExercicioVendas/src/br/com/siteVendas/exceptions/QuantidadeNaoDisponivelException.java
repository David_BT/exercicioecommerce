package br.com.siteVendas.exceptions;

public class QuantidadeNaoDisponivelException extends eCommerceException {

	public QuantidadeNaoDisponivelException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public QuantidadeNaoDisponivelException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public QuantidadeNaoDisponivelException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public QuantidadeNaoDisponivelException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public QuantidadeNaoDisponivelException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
