package br.com.siteVendas;

public class EstoqueProduto {
	
	private Produto produto;
	private Integer saldoEstoque;
	
	public EstoqueProduto() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the produto
	 */
	public Produto getProduto() {
		return produto;
	}

	/**
	 * @param produto the produto to set
	 */
	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	/**
	 * @return the saldoEstoque
	 */
	public Integer getSaldoEstoque() {
		return saldoEstoque;
	}

	/**
	 * @param saldoEstoque the saldoEstoque to set
	 */
	public void setSaldoEstoque(Integer saldoEstoque) {
		this.saldoEstoque = saldoEstoque;
	}
}
