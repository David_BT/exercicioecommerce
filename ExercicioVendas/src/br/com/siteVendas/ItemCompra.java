package br.com.siteVendas;

import java.math.BigDecimal;

public class ItemCompra {
	private Produto produto;
	private BigDecimal valorProduto;
	private Integer quantidadeProduto;
	
	public ItemCompra(Produto produto, BigDecimal valorProduto, Integer quantidadeProduto) {
		super();
		this.produto = produto;
		this.valorProduto = valorProduto;
		this.quantidadeProduto = quantidadeProduto;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public BigDecimal getValorProduto() {
		return valorProduto;
	}

	public void setValorProduto(BigDecimal valorProduto) {
		this.valorProduto = valorProduto;
	}

	public Integer getQuantidadeProduto() {
		return quantidadeProduto;
	}

	public void setQuantidadeProduto(Integer quantidadeProduto) {
		this.quantidadeProduto = quantidadeProduto;
	}
		
}
