package br.com.siteVendas;

import java.util.ArrayList;
import java.util.List;

import br.com.siteVendas.exceptions.ProdutoNaoEncontradoException;

public class BancoProduto {

	private List<EstoqueProduto> estoque;
	
	public BancoProduto() {
		super();
		estoque = new ArrayList<EstoqueProduto>();
	}

	public EstoqueProduto buscarProduto(Integer codigoProduto) throws ProdutoNaoEncontradoException {
		EstoqueProduto res = null;
		
		for (EstoqueProduto estoqueProduto : estoque) {
			if (estoqueProduto.getProduto().getCodigoProduto() == codigoProduto) {
				res = estoqueProduto;
			}
		}		
		
		if (res == null ){
			throw new ProdutoNaoEncontradoException("Produto pesquisado não encontrado");
			
		}
		return res;
	}
	
	public void removeStock(Integer productId, Integer amount) throws ProdutoNaoEncontradoException {
		
		EstoqueProduto stock = buscarProduto(productId);
		
		stock.setSaldoEstoque(stock.getSaldoEstoque() - amount);
	}
	
	public void addStock(Integer productId, Integer amount) throws ProdutoNaoEncontradoException {
		
		EstoqueProduto stock = buscarProduto(productId);
		
		stock.setSaldoEstoque(stock.getSaldoEstoque() - amount);
	}


	public List<EstoqueProduto> getEstoque() {
		return estoque;
	}

	public void setEstoque(List<EstoqueProduto> estoque) {
		this.estoque = estoque;
	}

}
