package br.com.siteVendas;

import java.util.ArrayList;
import java.util.List;

import br.com.siteVendas.enums.TipoDocumentoEnum;
import br.com.siteVendas.exceptions.ClienteNaoEncontradoException;
import br.com.siteVendas.exceptions.CpfInconsistenteException;
import br.com.siteVendas.exceptions.RGInconsistenteException;
import br.com.siteVendas.validadores.ValidadorDocumento;

public class BancoClientes {

	private List<Cliente> listaClientes;
	
	public BancoClientes() {
		super();
		
		listaClientes = new ArrayList<Cliente>();
		// TODO Auto-generated constructor stub
	}
	
	public void cadastrarCliente(Cliente cliente) throws RGInconsistenteException, CpfInconsistenteException{
		
		ValidadorDocumento.getInstace(TipoDocumentoEnum.RG).validadorDocumento(cliente.getRg());
		ValidadorDocumento.getInstace(TipoDocumentoEnum.CPF).validadorDocumento(cliente.getCpf());
		
		listaClientes.add(cliente);
	}
	
	public Cliente findClient(Cliente cliente) throws ClienteNaoEncontradoException {
		Cliente res = null;
		
		for (Cliente clienteFor : listaClientes) {
			if (cliente.getRg().equals(clienteFor.getRg()) ||
					cliente.getCpf().equals(clienteFor.getCpf())) {
				res = clienteFor;
			}
		}
		
		if (res == null) {
			throw new ClienteNaoEncontradoException("Não foi encontrado cliente com o seguinte RG/CPF: " +cliente.getRg() +"/" +cliente.getCpf());
		}
				
		return res;
	}
}
