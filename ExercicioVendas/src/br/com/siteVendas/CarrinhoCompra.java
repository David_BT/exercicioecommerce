package br.com.siteVendas;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import br.com.siteVendas.exceptions.ClienteNaoEncontradoException;
import br.com.siteVendas.exceptions.ProdutoNaoEncontradoException;
import br.com.siteVendas.exceptions.QuantidadeNaoDisponivelException;

public class CarrinhoCompra {
	
	private Cliente cliente;
	private List<ItemCarrinho> listaItens;
	private BigDecimal totalValue;
	
	private BancoClientes clientBank;
	private BancoProduto productBank;
	
	public CarrinhoCompra() {
		super();
		
		listaItens = new ArrayList<ItemCarrinho>();
	}
	
	public void logginCliente(Cliente cliente) throws ClienteNaoEncontradoException{
		
		this.cliente = clientBank.findClient(cliente);
	}
	
	public void addProduct(Integer productID, Integer amount) throws ProdutoNaoEncontradoException, QuantidadeNaoDisponivelException {
		
		EstoqueProduto stock = productBank.buscarProduto(productID);
		
		if (stock.getSaldoEstoque() > amount) {
			
			ItemCarrinho cartItem = new ItemCarrinho(stock.getProduto(), stock.getProduto().getValorProduto(), amount);
			totalValue = cartItem.getValorProduto().add(totalValue);
			
			productBank.removeStock(productID, amount);
			
		} else {
			throw new QuantidadeNaoDisponivelException("Quantidade pedida do produto não disponivel");
		}
	}
	
	
	// Getter & Setters
		
	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public List<ItemCarrinho> getListaItens() {
		return listaItens;
	}

	public void setListaItens(List<ItemCarrinho> listaItens) {
		this.listaItens = listaItens;
	}
}
